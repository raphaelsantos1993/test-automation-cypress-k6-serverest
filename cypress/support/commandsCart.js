Cypress.Commands.add('addProducts', (bodyProduct) => {
  cy.request({
    method: 'POST',
    url: Cypress.env("urlBase") + "/produtos",
    headers: {
      Authorization: Cypress.env('token')
    },
    body: bodyProduct
  }).then(({body:{message, _id }, status}) => {
    cy.wrap(_id).as('bodyIdProduct');
      expect(status).to.equal(201);
      expect(message).to.equal('Cadastro realizado com sucesso');
      expect(_id).to.not.be.null
      cy.wrap(_id).as('idProduct');
      cy.get('@idProduct').then((id) => {
        Cypress.env('productList', [...(Cypress.env('productList') || []), id]);
        cy.log('Lista de produtos:', JSON.stringify(Cypress.env('productList')));
    });
  });
});

Cypress.Commands.add('getProductId', (id) => {
  cy.request({
    method: 'GET',
    url: Cypress.env("urlBase") + "/produtos/"+ id,
    headers: {
      Authorization: Cypress.env('token')
    }
  }).then(({body:{ nome, descricao, quantidade, _id}, status}) => {
      expect(status).to.equal(200);
      expect(_id).to.not.be.null
      expect(nome).to.not.be.null
      expect(descricao).to.not.be.null
      expect(quantidade).to.not.be.null
      cy.wrap(quantidade).as('quantidadeProd');
  });
});


Cypress.Commands.add('registerCart', (bodyCard) => {
  cy.request({
    method: 'POST',
    url: Cypress.env("urlBase") + "/carrinhos",
    headers: {
      Authorization: Cypress.env('token')
    },
    body: bodyCard
  }).then(({body:{message, _id }, status}) => {
    cy.wrap(_id).as('bodyIdCard');
      expect(status).to.equal(201);
      expect(message).to.equal('Cadastro realizado com sucesso');
      expect(_id).to.not.be.null
      cy.wrap(_id).as('bodyIdCard');
      cy.get('@bodyIdCard').then((id) => {
        Cypress.env('IdCard', id);
        cy.log(Cypress.env('IdCard'));
    });
  });
});

Cypress.Commands.add('closeCart', () => {
  cy.request({
    method: 'DELETE',
    url: Cypress.env("urlBase") + "/carrinhos/concluir-compra",
    headers: {
      Authorization: Cypress.env('token')
    }
  }).then(({body:{message}, status}) => {
      expect(status).to.equal(200);
      expect(message).to.equal('Registro excluído com sucesso');
  });
});

