
Cypress.Commands.add('createUser', (bodyNewUser) => {
    cy.request({
      method: 'POST',
      url: Cypress.env("urlBase") + "/usuarios",
      body: bodyNewUser
    }).then(({body:{message, _id }, status, body}) => {
      cy.wrap(body).as('bodyCreateUser');
        expect(status).to.equal(201);
        expect(message).to.equal('Cadastro realizado com sucesso');
        expect(_id).to.not.be.null
    });

  });

  Cypress.Commands.add('createUserFail', (bodyNewUserFail) => {
    cy.request({
      method: 'POST',
      url: Cypress.env("urlBase") + "/usuarios",
      body: bodyNewUserFail,
      failOnStatusCode: false
    }).then(({body:{ message }, status}) => {
        expect(status).to.equal(400);
        expect(message).to.equal('Este email já está sendo usado');
    }); 
    
  });

  Cypress.Commands.add('createUserFailEmailFalse', (bodyNewUserFailEmailFalse) => {
    cy.request({
      method: 'POST',
      url: Cypress.env("urlBase") + "/usuarios",
      body: bodyNewUserFailEmailFalse,
      failOnStatusCode: false
    }).then(({body:{ email }, status}) => {
        expect(status).to.equal(400);
        expect(email).to.equal('email é obrigatório');
    }); 
    
  });

  Cypress.Commands.add('deleteUser', (id) => {
    cy.request({
      method: 'DELETE',
      url: Cypress.env("urlBase") + "/usuarios/" + id
    }).then(({body:{ message}, status}) => {
      expect(status).to.equal(200);
      expect(message).to.equal('Registro excluído com sucesso');
    });
  });

  Cypress.Commands.add('deleteUserFail', (id) => {
    cy.request({
      method: 'DELETE',
      url: Cypress.env("urlBase") + "/usuarios/" + id
    }).then(({body:{ message}, status}) => {
      expect(status).to.equal(200);
      expect(message).to.equal('Nenhum registro excluído');
    });
  });

  Cypress.Commands.add('editUser', (id, bodyEdit) => {
    cy.request({
      method: 'PUT',
      url: Cypress.env("urlBase") + "/usuarios/" + id,
      body: bodyEdit
    }).then(({body:{ message }, status}) => {
        expect(status).to.equal(200);
        expect(message).to.equal('Registro alterado com sucesso');
    });
  });

  Cypress.Commands.add('editUserRegisteredEmail', (id, bodyEdit) => {
    cy.request({
      method: 'PUT',
      url: Cypress.env("urlBase") + "/usuarios/" + id,
      body: bodyEdit,
      failOnStatusCode: false
    }).then(({body:{ message }, status}) => {
        expect(status).to.equal(400);
        expect(message).to.equal('Este email já está sendo usado');
    });
  });

  Cypress.Commands.add('editUserFail', (id, bodyEdit) => {
    cy.request({
      method: 'PUT',
      url: Cypress.env("urlBase") + "/usuarios/" + id,
      body: bodyEdit
    }).then(({body:{ message }, status}) => {
        expect(status).to.equal(400);
        expect(message).to.equal('Id não Encontrado');
    });
  });

  Cypress.Commands.add('getListUsers', () => {
    cy.request({
      method: 'GET',
      url: Cypress.env("urlBase") + "/usuarios"
    }).then(({body:{ quantidade , usuarios}, status, body}) => {
      cy.wrap(body).as('bodyGetUserList');
        expect(status).to.equal(200);
        expect(usuarios).to.be.an('array');
        expect(quantidade).to.be.a('number');
        expect(quantidade).to.be.at.least(0);
    });
  });

  Cypress.Commands.add('getUserId', (id) => {
    cy.request({
      method: 'GET',
      url: Cypress.env("urlBase") + "/usuarios",
      qs: {
        _id: id
      }
    }).then(({body:{ quantidade , usuarios}, status}) => {
        expect(status).to.equal(200);
        expect(usuarios).to.be.an('array');
        expect(quantidade).to.be.a('number');
        expect(quantidade).to.be.at.least(0);
    });
  });

  Cypress.Commands.add('getUserToId', (id) => {
    cy.request({
      method: 'GET',
      url: Cypress.env("urlBase") + "/usuarios/" + id
    }).then(({body:{ nome , email, password, administrador, _id }, status}) => {
        expect(status).to.equal(200);
        expect(nome).to.not.be.null;
        expect(email).to.not.be.null;
        expect(password).to.not.be.null;
        expect(administrador).to.not.be.null;
        expect(_id).to.not.be.null;
    });
  });

  Cypress.Commands.add('getUserToIdFail', (id) => {
    cy.request({
      method: 'GET',
      url: Cypress.env("urlBase") + "/usuarios/" + id,
      failOnStatusCode: false
    }).then(({body:{ message }, status}) => {
        expect(status).to.equal(400);
        expect(message).to.equal('Usuário não encontrado');
    });
  });

  Cypress.Commands.add('getUserEmail', (email) => {
    cy.request({
      method: 'GET',
      url: Cypress.env("urlBase") + "/usuarios",
      qs: {
        email: email
      }
    }).then(({body:{ quantidade , usuarios}, status}) => {
        expect(status).to.equal(200);
        expect(usuarios).to.be.an('array');
        expect(quantidade).to.be.a('number');
        expect(quantidade).to.be.at.least(0);
    });
  });

  Cypress.Commands.add('getUserPassword', (password) => {
    cy.request({
      method: 'GET',
      url: Cypress.env("urlBase") + "/usuarios",
      qs: {
        password: password
      }
    }).then(({body:{ quantidade , usuarios}, status}) => {
        expect(status).to.equal(200);
        expect(usuarios).to.be.an('array');
        expect(quantidade).to.be.a('number');
        expect(quantidade).to.be.at.least(0);
    });
  });

  Cypress.Commands.add('getUserAdministrador', (administrador) => {
    cy.request({
      method: 'GET',
      url: Cypress.env("urlBase") + "/usuarios",
      qs: {
        administrador: administrador
      }
    }).then(({body:{ quantidade , usuarios}, status}) => {
        expect(status).to.equal(200);
        expect(usuarios).to.be.an('array');
        expect(quantidade).to.be.a('number');
        expect(quantidade).to.be.at.least(0);
    });
  });

  Cypress.Commands.add('getUserName', (name) => {
    cy.request({
      method: 'GET',
      url: Cypress.env("urlBase") + "/usuarios",
      qs: {
        nome: name
      }
    }).then(({body:{ quantidade , usuarios}, status}) => {
        expect(status).to.equal(200);
        expect(usuarios).to.be.an('array');
        expect(quantidade).to.be.a('number');
        expect(quantidade).to.be.at.least(0);
    });
  });

Cypress.Commands.add('validateResponseSchema', (response, responseSchema) => {
    expect(response).to.be.jsonSchema(responseSchema);
});
