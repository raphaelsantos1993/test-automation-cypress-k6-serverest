Cypress.Commands.add('loginUser', (email, password) => {
  cy.request({
    method: 'POST',
    url: Cypress.env("urlBase") + "/login",
    body: {
      email: email,
      password: password
    }
  }).then(({body:{message, authorization }, status, body}) => {
    cy.wrap(authorization).as('tokenLogin');
    cy.wrap(body).as('bodyToken');
      expect(status).to.equal(200);
      expect(message).to.equal('Login realizado com sucesso');
      expect(authorization).to.not.be.null
  });
    cy.get('@tokenLogin').then((bodytokenLogin) => {
      Cypress.env("token", bodytokenLogin)
  });

});