import { faker } from '@faker-js/faker';

export let product1 = (qtaProdEstoque1) => ({
    "nome": faker.commerce.productName(),
    "preco": 500,
    "descricao": "Mouse",
    "quantidade": qtaProdEstoque1
});

  export let product2 = (qtaProdEstoque2) => ({
    "nome": faker.commerce.productName(),
    "preco": 500,
    "descricao": "Teclado",
    "quantidade": qtaProdEstoque2
});

  export let cart = (idProduto1, idProduto2, qtaProd1, qtaProd2) => ({
    "produtos": [
      {
        "idProduto": idProduto1,
        "quantidade": qtaProd1
      },
      {
        "idProduto": idProduto2,
        "quantidade": qtaProd2
      }
    ]
  });