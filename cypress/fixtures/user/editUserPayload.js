let random = require('random-name')
const nameFull = random.first() + random.middle() + random.last()

export let editUser = {
    "nome": "Fulano da Silva",
    "email": nameFull + "@qa.com.br",
    "password": "teste",
    "administrador": "true"
}

export let editUserRegisteredEmail = {
    "nome": "Fulano da Silva",
    "email": "beltranno@qa.com.br",
    "password": "teste",
    "administrador": "true"
}