let random = require('random-name')
const nameFull = random.first() + random.middle() + random.last()

export let userSuccess = {
  "nome": nameFull,
  "email": nameFull+ "@qa.com.br",
  "password": "teste",
  "administrador": "true"
}

export let userFail = {
    "nome": "Fulano da Silva",
    "email": "beltrano@qa.com.br",
    "password": "teste",
    "administrador": "true"
}

export let userEmailIcorrect = {
  "nome": nameFull,
  "email": nameFull + "qa.com.br",
  "password": "teste",
  "administrador": "true"
}
