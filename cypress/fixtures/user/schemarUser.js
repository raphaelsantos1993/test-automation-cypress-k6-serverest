export const responseSchemaUser = {
    type: 'object',
    properties: {
      message: { type: 'string' },
      _id: { type: 'string' }
    },
    required: ['message', '_id']
  };

  export const responseSchemaUserList = {
    type: 'object',
    properties: {
        quantidade: { type: 'integer' },
        usuarios: {
            type: 'array',
            items: {} // Define um array vazio
        }
    },
    required: ['quantidade', 'usuarios']
};

export const responseSchemaLogin = {
  type: 'object',
  properties: {
    message: { type: 'string' },
    authorization: { type: 'string' }
  },
  required: ['message', 'authorization']
};