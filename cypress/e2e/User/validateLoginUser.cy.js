const payloadNewUser = require('../../fixtures/user/newUserPayload')

describe('Validar login usuário - API serverest.dev ', { tags: '@user' }, () => {

    it('Validar login usuário Sucesso', { tags: ['@user', '@smoke'] }, () => {
        const user = payloadNewUser.userSuccess;
        cy.createUser(user);
        cy.loginUser(user.email, user.password);
    });

});