const payloadNewUser = require('../../fixtures/user/newUserPayload')

const user = payloadNewUser.userSuccess;

describe('Validar Busca usuário por Id especifico - API serverest.dev ', { tags: '@user' }, () => {

    it('Validar Busca usuário por Id especifico', { tags: ['@user'] }, () => {
        cy.createUser(payloadNewUser.userSuccess);
        cy.get('@bodyCreateUser').then((bodyCreateUser) => {
            cy.getUserToId(bodyCreateUser._id);
        });
    });

    it('Validar Busca usuário por Id especifico não encontrado', { tags: ['@user'] }, () => {
            cy.getUserToIdFail('123456');
    });

});