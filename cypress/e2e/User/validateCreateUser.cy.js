const payloadNewUser = require('../../fixtures/user/newUserPayload')

describe('Validar criação de Novo usuário - API serverest.dev ', { tags: '@user' }, () => {
  
    it('Validar criação novo usuario sucesso', { tags: ['@user'] }, () => {
        cy.createUser(payloadNewUser.userSuccess);
    });

    it('Validar criação usuario já existente', { tags: ['@user'] }, () => {
        cy.createUserFail(payloadNewUser.userFail);
    });

    it('Validar criação usuario já email incorrento', { tags: ['@user'] }, () => {
        cy.createUserFailEmailFalse(payloadNewUser.createUserFailEmailFalse);
    });
    
  });