const payloadNewUser = require('../../fixtures/user/newUserPayload')

describe('Validar deleção usuário - API serverest.dev ', { tags: '@user' }, () => {
  
    it('Validar deleção usuário', { tags: ['@user'] }, () => {
        cy.createUser(payloadNewUser.userSuccess);
        cy.get('@bodyCreateUser').then((bodyCreateUser) => {
            cy.deleteUser(bodyCreateUser._id);
        });
    });

    it('Validar deleção usuário invalido', { tags: ['@user'] }, () => {
            cy.deleteUserFail('123456');
    });
  });