const payloadNewUser = require('../../fixtures/user/newUserPayload')

const user = payloadNewUser.userSuccess;

describe('Validar Busca usuários - API serverest.dev ', { tags: '@user' }, () => {

    it('Validar busca lista de usuarios', { tags: ['@user'] }, () => {
        cy.getListUsers();
    });

    it('Validar busca usuario por id', { tags: ['@user'] }, () => {
        cy.createUser(payloadNewUser.userSuccess);
        cy.get('@bodyCreateUser').then((bodyCreateUser) => {
            cy.getUserId(bodyCreateUser._id);
        });
    });

    it('Validar busca usuario por nome', { tags: ['@user'] }, () => {
        cy.getUserName(user.nome);
    });

    it('Validar busca usuario por email', { tags: ['@user'] }, () => {
        cy.getUserEmail(user.email);
    });

    it('Validar busca usuario por password', { tags: ['@user'] }, () => {
        cy.getUserPassword(user.password);
    });

    it('Validar busca usuario por administrador', { tags: ['@user'] }, () => {
        cy.getUserAdministrador(user.administrador);
    });
    
  });