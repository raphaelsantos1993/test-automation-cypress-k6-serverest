const payloadNewUser = require('../../fixtures/user/newUserPayload')
const payloadEditUser = require('../../fixtures/user/editUserPayload')

const user = payloadNewUser.userSuccess;

describe('Validar Edição de usuário - API serverest.dev ', { tags: '@user' }, () => {

    it('Validar Edição de usuário ', { tags: ['@user'] }, () => {
        cy.createUser(payloadNewUser.userSuccess);
        cy.get('@bodyCreateUser').then((bodyCreateUser) => {
            cy.editUser(bodyCreateUser._id, payloadEditUser.editUser);
        });
    });

    it.skip('Validar Edição de usuário Id incorreto', { tags: ['@user'] }, () => {
        // Correção Bug Api - (Put realiza cadastro de Novo usuario ao informa id incorreto)    
        cy.editUserFail('123456', payloadEditUser.editUser);
    });

    it('Validar Edição de usuário com email ja cadastrado', { tags: ['@user'] }, () => {
        cy.createUser(payloadNewUser.userSuccess);
        cy.get('@bodyCreateUser').then((bodyCreateUser) => {
            cy.editUserRegisteredEmail(bodyCreateUser._id, payloadEditUser.editUserRegisteredEmail);
        });
    });


});