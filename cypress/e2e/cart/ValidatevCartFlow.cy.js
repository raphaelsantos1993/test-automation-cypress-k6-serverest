const payloadNewUser = require('../../fixtures/user/newUserPayload')
const payloadProduct = require('../../fixtures/cart/cartPayload')
const user = payloadNewUser.userSuccess;

const qtaProdEstoque1 = 100;
const qtaProdEstoque2 = 50;
const qtaProd1 = 10;
const qtaProd2 = 7;

describe('Validar Fluxo de criação de um carrinho :- API serverest.dev ', { tags: '@cart' }, () => {

    it('Validar criação novo usuario sucesso', { tags: ['@cart'] }, () => {
        cy.createUser(user);
    });

    it('Validar login usuário Sucesso', { tags: ['@cart'] }, () => {
        cy.loginUser(user.email, user.password);
    });

    it('Validar cadastro de produtos', { tags: ['@cart'] }, () => {
        cy.addProducts(payloadProduct.product1(qtaProdEstoque1));
        cy.addProducts(payloadProduct.product2(qtaProdEstoque2));
    });

    it('Validar cadastro do carrinho', { tags: ['@cart'] }, () => {
        const id1 = (Cypress.env('productList') || [])[0];
        const id2 = (Cypress.env('productList') || [])[1];
        cy.registerCart(payloadProduct.cart(id1, id2, qtaProd1, qtaProd2))
    });

    it('Validar Finalização do carrinho', { tags: ['@cart'] }, () => {
        cy.closeCart();
    });

    it('Valide se as quantidades adicionadas foram debitadas da quantidade disponível dos produtos', { tags: ['@Cart', '@smoke'] }, () => {
        const id1 = (Cypress.env('productList') || [])[0];
        const id2 = (Cypress.env('productList') || [])[1];

        cy.getProductId(id1);
        cy.get('@quantidadeProd').then((qtaProd) => {
            expect(qtaProdEstoque1 - qtaProd1).to.equal(qtaProd);
        });

        cy.getProductId(id2);
        cy.get('@quantidadeProd').then((qtaProd) => {
            expect(qtaProdEstoque2 - qtaProd2).to.equal(qtaProd);
        });
        
    });

});