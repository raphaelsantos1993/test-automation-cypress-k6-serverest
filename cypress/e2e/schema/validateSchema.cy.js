const schemaNewUser = require('../../fixtures/user/schemarUser')
const payloadNewUser = require('../../fixtures/user/newUserPayload')

const user = payloadNewUser.userSuccess;

describe('Validar schema do response/contrato retornado - API serverest.dev ', { tags: ['@schema'] }, () => {
  
    before(() => { 
        cy.createUser(user);
      });

    it('Validar schema criação novo usuario sucesso', { tags: ['@schema'] }, () => {
        cy.log(payloadNewUser.userSuccess.email)
        cy.get('@bodyCreateUser').then((bodyCreateUser) => {
            cy.validateResponseSchema(bodyCreateUser, schemaNewUser.responseSchemaUser );
        });
    });

    it('Validar schema lista de usuarios', { tags: ['@schema'] }, () => {
        cy.getListUsers();
        cy.get('@bodyGetUserList').then((bodyGetUserList) => {
            cy.validateResponseSchema(bodyGetUserList, schemaNewUser.responseSchemaUserList );
        });
    });

    it('Validar schema login de usuario', { tags: ['@schema'] }, () => {
        cy.loginUser(user.email, user.password);
        cy.get('@bodyToken').then((bodytokenLogin) => {
            cy.validateResponseSchema(bodytokenLogin, schemaNewUser.responseSchemaLogin );
        });
    });
    
  });