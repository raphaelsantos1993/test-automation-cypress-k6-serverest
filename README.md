# Projeto Automação (Cypress/K6) - Desafio API ServeRest

## Descrição

Este projeto em Cypress foi desenvolvido para realizar testes automatizados na API ServeRest, que simula uma API de uma loja para aprendizado. O objetivo é validar o funcionamento correto das principais funcionalidades da API, como login, usuários, produtos e carrinho.

A API ServeRest está hospedada em um contêiner Docker, cujo link para o repositório é [ServeRest](https://github.com/ServeRest/ServeRest).


## Tecnologias Utilizadas

- [Cypress](https://docs.cypress.io/guides/overview/why-cypress.html)
- [JavaScript](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide)
- [Docker](https://docs.docker.com/)
- [K6](https://k6.io/docs/getting-started/running-k6/)


- O Cypress oferece cobertura abrangente de testes de ponta a ponta, com sintaxe amigável e rápida execução, proporcionando análises detalhadas dos resultados.

- O K6 destaca-se em testes de carga e desempenho, simulando cenários reais e integrando-se ao Cypress para uma avaliação profunda da performance.

- Ambas as ferramentas são suportadas por comunidades ativas, garantindo qualidade, desempenho e confiabilidade ao longo do ciclo de desenvolvimento.

- O Cypress e o K6 se destacam por utilizarem JavaScript, uma linguagem interpretada, em comparação com ferramentas como Java com RestAssured ou JMeter, que requerem linguagens compiladas. Essa diferença torna o desenvolvimento de testes mais ágil e flexível, pois o JavaScript não exige um processo de compilação prévio à execução dos testes.

## Pré-requisitos

Antes de executar o projeto, certifique-se de ter as seguintes ferramentas instaladas em seu sistema:

- [Node.js](https://nodejs.org/): O Node.js é necessário para executar o projeto. Você pode baixar e instalar a versão adequada para o seu sistema operacional a partir do site oficial.

- [Docker](https://www.docker.com/): O Docker é usado para hospedar a API ServeRest em um contêiner. Certifique-se de ter o Docker instalado e configurado de acordo com o sistema operacional que você está utilizando.

- [Cypress](https://www.cypress.io/): O Cypress é uma ferramenta de automação de testes end-to-end para aplicações web. Certifique-se de ter o Cypress instalado globalmente em seu sistema.

- [K6](https://k6.io/): O K6 é uma ferramenta de teste de carga e desempenho. Certifique-se de ter o K6 instalado globalmente em seu sistema.
[K6-Install](https://k6.io/docs/get-started/installation/)

## Como Executar o Projeto

#### Navegue até o diretório do projeto:

  ```bash
cd neoway-test-automation-cypress-k6
```

## Como Executar os Testes

### Passos
1. Clone este repositório para o seu ambiente local, execute o seguinte comando no terminal:
   ```bash
   git clone https://gitlab.com/raphaelsantos1993/test-automation-cypress-k6-serverest.git
   ```
2. Instale as dependências do projeto com npm, execute o seguinte comando no terminal::
   ```bash
   npm install
   ```  
3. Baixe o container docker da Api, execute o seguinte comando no terminal:
   ```bash
   docker pull paulogoncalvesbh/serverest
   ```  
4. Baixe o container docker da Api, execute o seguinte comando no terminal:
   ```bash
   docker pull paulogoncalvesbh/serverest
   ```  
5. Suba o container localmente na porta 3000, execute o seguinte comando no terminal:
   ```bash
   docker run -p 3000:3000 paulogoncalvesbh/serverest:latest
   ```  
6. Para executar os cenarios de testes Funcionais na Automação **(Cypress)** use os seguintes comandos:

**test:open**: Abre o Cypress no modo gráfico. Para executar este comando, abra o terminal e digite:
 ```bash
npm run test:open
   ```  

 **test:headless**: Executa o Cypress em modo headless, sem interface gráfica. Para executar este comando, abra o terminal e digite:
 ```bash
npm run test:headless
   ```  

 **test:smoke**: Executa testes marcados como "smoke tests". Para executar este comando, abra o terminal e digite:
 ```bash
npm run test:smoke
npm run test:cart
npm run test:user
npm run test:schema
   ```  

--> **@smoke** => Tag adicionada para rodar teste de fumaça login com sucesso
--> **@cart** => Tag adicionada para rodar Fluxo de criação de um carrinho
--> **@user** => Tag adicionada para rodar Fluxo login de usuario
--> **@schema** => Tag adicionada para rodar Fluxo schema do response/contrato retornado pela Api

**Obsevação**
Você pode utilizar tags para filtrar os testes a serem executados. Aqui está um exemplo de como você pode usar tags com o Cypress:
```javascript
describe('Testes do carrinho de compras', () => {
  it('Adicionar item ao carrinho - @smoke', () => {
    // Implementação dos testes para adicionar item ao carrinho
  });

  it('Atualizar quantidade do item no carrinho - @regression', () => {
    // Implementação dos testes para atualizar a quantidade do item no carrinho
  });
});
```
Para executar com filtro da tag **@smoke**, use esse commando no cmd:
 ```bash
npx cypress run --env grepTags=@smoke
   ```  

 **test:allure**:O comando "test:allureServer" é usado para iniciar o Allure Server, que permite visualizar os relatórios gerados pelo Allure Framework. Para executar este comando, abra o terminal e digite:
 ```bash
npm run test:allure
   ```  

**obs: Comando principal para rodar os Testes Funcionais**

**test:allureServenpm**: Gera um relatório Allure, em um sevidor local. Para executar este comando diretamente pela dependencia [llure-commandline](https://www.npmjs.com/package/allure-commandline), Para executar este comando, abra o terminal e digite:
 ```bash
npm run test:allureServenpm
   ```  

**Em caso de erro seguir para proximo commando test:allureServe**

**test:allureServe**: Gera um relatório Allure, em um sevidor local. Para executar este comando, abra o terminal e digite:
 ```bash
npm run test:allureServe
   ```  
**Observação**: Para usar o comando "test:allureServer", é necessário ter o Allure Command-Line instalado na máquina onde os testes estão sendo executados. Você pode instalar o [Allure Command-Line - Instalação](https://github.com/allure-framework/allure2)

- **Instalar o Java JDK**
Primeiro, instale o Java JDK globalmente em sua máquina seguindo as instruções adequadas para o seu sistema operacional. Você pode baixar o JDK do site oficial da Oracle ou de fontes confiáveis.[Allure Command-Line - Instalação](https://github.com/allure-framework/allure2)

-**Instalar o Allure Commad-Line**
Siga a documentação do repositorio para instalar globalmente de acordo com o sistema operacional 

7. Para executar os cenarios de testes Não na Automação **(K6)** use os seguintes comandos:

**test:k6-carga**:para executar um teste de carga utilizando o K6. Ele muda o diretório para k6/scenarios e em seguida executa o script createUser.js usando o K6 para simular uma carga de usuários na aplicação.
 ```bash
npm run test:k6-carga
   ```  

## Estrutura do Projeto

```sql
O projeto está estruturado da seguinte forma:

neoway-test-automation-cypress-k6/
│
├── cypress/                       // Pasta principal do Cypress
│   │
│   ├── e2e/                       // Testes end-to-end
│   │   │
│   │   ├── cart/                  // Testes relacionados ao carrinho de compras
│   │   │   │
│   │   │   └── ValidateCartFlow.cy  // Validação do fluxo do carrinho
│   │   │
│   │   └── user/                  // Testes relacionados aos usuários
│   │       │
│   │       ├── validateCreateUser.cy  // Validação de criação de usuários
│   │       ├── validateDeleteUser.cy  // Validação de exclusão de usuários
│   │       ├── validateEditUser.cy    // Validação de edição de usuários
│   │       ├── validateGetUser.cy     // Validação de obtenção de usuários
│   │       ├── validateGetUserId.cy   // Validação de obtenção de usuário específico
│   │       ├── validateLoginUser.cy   // Validação de login de usuários
│   │       └── validateSchema.cy      // Validação de esquema de dados retornados da API ServeRest
│   │
│   ├── fixtures/                  // Dados para os testes
│   │   │
│   │   ├── cart/                  // Dados relacionados ao carrinho de compras
│   │   │   │
│   │   │   └── cartPayload.json   // Dados de teste do carrinho
│   │   │
│   │   └── user/                  // Dados relacionados aos usuários
│   │       │
│   │       ├── editUserPayload.json  // Dados para edição de usuários
│   │       ├── newUserPayload.json  // Dados para criação de novos usuários
│   │       └── schemaUser.json      // Esquema de dados do usuário
│   │
│   └── support/                   // Suporte aos testes
│       │
│       ├── commands/              // Comandos personalizados do Cypress
│       │   │
│       │   ├── commandsCart.js    // Comandos para o carrinho
│       │   ├── commandsLogin.js   // Comandos para login
│       │   └── commandsUser.js    // Comandos para usuários
│       │
│       └── e2e.js                 // Configurações ou scripts adicionais para os testes end-to-end
│
├── k6/                            // Scripts de teste de desempenho com o K6
│   │
│   ├── htmlReport/                // Relatórios em HTML gerados pelo K6
│   ├── lib/                       // Bibliotecas auxiliares
│   │   └── randomEmailGenerator.js  // Gerador de emails aleatórios
│   │
│   └── scenarios/                 // Scripts de cenários de teste com o K6
│       └── createUser.js          // Script de teste de carga de criação de usuários
│
├── node_modules/                  // Módulos do Node.js
├── .gitignore                      // Arquivo de configuração para o Git
├── cypress.config                  // Configurações do Cypress
├── cypress.env.json                // Variáveis de ambiente do Cypress
├── package-lock.json               // Informações detalhadas das dependências do projeto
└── package.json                    // Informações e dependências do projeto
```

## Estrutura do Projeto

1. **cypress/**
   - **e2e/**
     - **cart/**
       - `ValidateCartFlow.cy`: Validação do fluxo do carrinho.
     - **user/**
       - `validateCreateUser.cy`: Validação de criação de usuários.
       - `validateDeleteUser.cy`: Validação de exclusão de usuários.
       - `validateEditUser.cy`: Validação de edição de usuários.
       - `validateGetUser.cy`: Validação de obtenção de usuários.
       - `validateGetUserId.cy`: Validação de obtenção de usuário específico.
       - `validateLoginUser.cy`: Validação de login de usuários.
   - **fixtures/**
     - **cart/**
       - `cartPayload.json`: Dados de teste do carrinho.
     - **user/**
       - `editUserPayload.json`: Dados para edição de usuários.
       - `newUserPayload.json`: Dados para criação de novos usuários.
       - `schemaUser.json`: Esquema de dados do usuário.
   - **support/**
     - **commands/**
       - `commandsCart.js`: Comandos para o carrinho.
       - `commandsLogin.js`: Comandos para login.
       - `commandsUser.js`: Comandos para usuários.
     - `e2e.js`: Configurações ou scripts adicionais para os testes end-to-end.

2. **k6/**
   - **htmlReport/**: Relatórios HTML gerados pelos testes de desempenho.
   - **lib/**
     - `randomEmailGenerator.js`: Script para gerar emails aleatórios.
   - **scenarios/**
     - `createUser.js`: Script de teste de carga de criação de usuários.

3. **node_modules/**: Dependências instaladas pelo npm.
4. **.gitignore**: Especifica quais arquivos e pastas devem ser ignorados pelo Git.
5. **cypress.config**: Configurações do Cypress.
6. **cypress.env.json**: Variáveis de ambiente do Cypress.
7. **package-lock.json**: Versões específicas das dependências do projeto.
8. **package.json**: Informações sobre o projeto e suas dependências.


## Considerações Finais

Este projeto é parte de um desafio de automação de testes e foi desenvolvido com o intuito de garantir a qualidade e o correto funcionamento da API ServeRest.

--------------------------------------------------------------------------------------------------------------------

## Cenarios de Teste Ultilizado BDD:

```gherkin
Funcionalidade: Autenticação de Usuário

**Cenário**: Login com credenciais válidas
Dado que estou na página de login
Quando eu envio uma requisição POST para '/login' com o seguinte corpo:
"""
{
  "email": "beltranbod@qa.com.br",
  "password": "teste"
}
"""
Então devo receber um código de status 200
E devo receber um token de acesso

Cenário Login com e-mail inválido
Dado que estou na página de login
Quando eu envio uma requisição POST para '/login' com o seguinte corpo:
"""
{
  "email": "email_invalido@qa.com",
  "password": "teste"
}
"""
Então devo receber um código de status 401
E devo receber uma mensagem de erro informando que as credenciais estão incorretas

Cenário Login com senha inválida
Dado que estou na página de login
Quando eu envio uma requisição POST para '/login' com o seguinte corpo:
"""
{
  "email": "beltranbod@qa.com.br",
  "password": "senha_incorreta"
}
"""
Então devo receber um código de status 401
E devo receber uma mensagem de erro informando que as credenciais estão incorretas

Cenário Login sem informar e-mail
Dado que estou na página de login
Quando eu envio uma requisição POST para '/login' sem informar o e-mail
Então devo receber um código de status 400
E devo receber uma mensagem de erro informando que o e-mail é obrigatório

Cenário: Login sem informar senha
Dado que estou na página de login
Quando eu envio uma requisição POST para '/login' sem informar a senha
Então devo receber um código de status 400
E devo receber uma mensagem de erro informando que a senha é obrigatória

Cenário: Token de acesso inválido
Dado que estou autenticado com um token de acesso inválido
Quando eu envio uma requisição POST para '/login' com o token de acesso inválido
Então devo receber um código de status 401
E devo receber uma mensagem de erro informando que o token de acesso é inválido

Cenário: Token de acesso nulo
Dado que estou autenticado sem um token de acesso válido
Quando eu envio uma requisição POST para '/login' sem o token de acesso
Então devo receber um código de status 401
E devo receber uma mensagem de erro informando que o token de acesso é obrigatório

Cenário: Token de acesso expirado
Dado que estou autenticado com um token de acesso expirado
Quando eu envio uma requisição POST para '/login' com o token de acesso expirado
Então devo receber um código de status 401
E devo receber uma mensagem de erro informando que o token de acesso expirou

```
--------------------------------------------------------------------------------------------------------------------

## Pontos de Melhoria da Documentação

Documentação Fictícia: Projeto de Enriquecimento de Dados de Livros
Visão Geral
O Projeto de Enriquecimento de Dados de Livros visa aprimorar as informações fornecidas pelo cliente por meio da incorporação de
dados adicionais da base de dados de livros existente. O cliente disponibilizará um arquivo contendo títulos de livros e suas
edições, enquanto o projeto utilizará esses dados como entrada para enriquecimento e por final irá entregar um novo arquivo para o
cliente com mais detalhes dos livros.
Objetivos
- Ampliar as informações fornecidas pelo cliente através da complementação com dados adicionais da base de dados de
livros.
- Aprimorar a qualidade e a abrangência das informações disponíveis sobre os livros fornecidos pelo cliente.
- Facilitar o acesso e a busca por informações detalhadas sobre os livros em questão.
Requisitos Funcionais
- Leitura do Arquivo do Cliente: O sistema deve ser capaz de ler o arquivo fornecido pelo cliente, contendo os títulos e as
edições dos livros.
- Consulta à Base de Dados de Livros: Realizar consultas à base de dados de livros para obter informações adicionais
com base nos títulos e edições fornecidos pelo cliente.
- Enriquecimento de Dados: Com base nas consultas realizadas, o sistema deve enriquecer os dados fornecidos pelo
cliente com informações adicionais, como autor, editora, data de publicação, sinopse, e outras informações sobre de livros.
- Entrega dos Dados Enriquecidos: Após o processo de enriquecimento, os dados serão compilados em um novo arquivo
e entregues ao cliente para referência futura. O arquivo estará em um formato adequado e acessível, facilitando a
integração com o sistema do cliente.

### Resumo:

Mais Detalhes nos Dados Extras:

1. Explicar que tipo de informações adicionais vamos incluir nos livros, como quem escreveu, data de publicação, sinopse, etc.
Como Pegamos Esses Dados Extras:

2. Descrever de onde vamos obter esses detalhes extras, como de um site específico, de uma base de dados existente, ou de fontes como bibliotecas.
Como Saber se Deu Certo:

3. Definir como vamos medir o sucesso do projeto, por exemplo, verificando se há mais informações nos livros ou se os usuários encontram mais facilmente o que procuram.
Explica Melhor Como Vai Funcionar o Lance de Melhorar os Dados:

4. Detalhar o processo de enriquecimento de dados, seja por meio de um sistema automático ou manualmente pelos colaboradores.
Como Vai Ser a Entrega dos Dados Melhorados:

5. Esclarecer como os dados enriquecidos serão entregues ao cliente, seja por meio de um arquivo digital, atualização de um site, ou relatório impresso.
Pensar em Como Aprender com o Feedback:

6. Planejar como vamos coletar feedback do cliente após a entrega dos dados, seja por meio de pesquisas, reuniões de discussão, entre outros métodos.

--------------------------------------------------------------------------------------------------------------------

## Relório Teste de Carga

**Cenário 1**
Performance de listagem de usuários cadastrados:
Na funcionalidade de usuários cadastrados queremos saber como está o desempenho da API, quando
utilizamos 5 usuários simultâneos durante 5 minutos.
Gostaríamos de um relatório de desempenho com dados técnicos obtidos pela sua execução dos scripts de
performance e uma análise sua do resultado.

## Relatório de Desempenho

### Resumo dos Resultados

- **Checks**: 100.00% Passados (1814 de 1814)
- **Dados Recebidos**: 1.0 MB a uma taxa de 2.5 kB/s
- **Dados Enviados**: 485 kB a uma taxa de 1.2 kB/s
- **Taxa de Transferência de Dados**: Média de 8.2 (min=3.727273, max=27.333333)
- **Erros**: 0.00% (0 de 1814)
- **Requisições HTTP**: 1814 completas
- **Tempo de Execução**: 7 minutos e 0.5 segundos

## Análise dos Resultados

### Desempenho Geral

O teste foi concluído com sucesso, atingindo 100% de checks passados e sem erros registrados durante a execução. A taxa de transferência de dados variou de 3.727273 a 27.333333, com uma média de 8.2, indicando uma boa performance em termos de comunicação.

## Tempo de Resposta
- **Tempo Médio de Resposta:** 9.209482 ms
- **Tempo Mínimo de Resposta**: 3 ms
- **Tempo Máximo de Resposta**: 22 ms
- **Percentil 90% de Tempo de Resposta**: 11 ms
- **Percentil 95% de Tempo de Resposta**: 12 ms
- Os tempos de resposta das requisições HTTP mostram um bom desempenho, com a maioria das requisições (90%) respondendo dentro de 11 ms e 95% das requisições dentro de 12 ms.

## Taxa de Erros
Durante a execução dos testes, não foram registrados erros nas requisições HTTP, demonstrando a estabilidade do sistema sob teste.

## Conclusão
Os resultados do teste de desempenho mostram que a API em um ambiente Docker é estável, sem erros em 1814 iterações. As métricas de transferência de dados e tempo de resposta estão satisfatórias.