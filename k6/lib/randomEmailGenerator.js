export default class RandomEmailGenerator {
    constructor() {
        this.domainList = ['gmail.com', 'yahoo.com', 'hotmail.com', 'example.com'];
    }

    generateEmail() {
        const username = this.generateRandomString(10); // Gera um nome de usuário aleatório com 10 caracteres
        const domain = this.domainList[Math.floor(Math.random() * this.domainList.length)]; // Escolhe um domínio aleatório da lista

        return `${username}@${domain}`;
    }

    generateRandomString(length) {
        const characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
        let randomString = '';

        for (let i = 0; i < length; i++) {
            randomString += characters.charAt(Math.floor(Math.random() * characters.length));
        }

        return randomString;
    }
}
