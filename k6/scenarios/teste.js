import http from 'k6/http';
import { check, sleep } from 'k6';

export let options = {
  vus: 10, // número de usuários virtuais simulados
  duration: '30s', // duração do teste
};

export default function () {
  let url = 'http://example.com/api/users'; // substitua pelo URL real da sua API
  let payload = JSON.stringify({
    name: 'John Doe',
    email: 'johndoe@example.com',
  });

  let params = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  let res = http.post(url, payload, params);
  check(res, {
    'status is 201': (r) => r.status === 201,
  });

  sleep(1); // espera 1 segundo entre as solicitações
}
