import http from 'k6/http';
import { check, sleep } from 'k6';
import { Rate, Trend, Counter, Gauge } from 'k6/metrics';
import { htmlReport } from "https://raw.githubusercontent.com/benc-uk/k6-reporter/main/dist/bundle.js";
import RandomEmailGenerator from '../lib/randomEmailGenerator.js';

export function handleSummary(data) {
    console.log('Resumo do Teste:');
    console.log('Métricas do Teste:');
    console.log(`Erros: ${errorRate.count()}, Tempo de Resposta: ${responseTime.mean()}ms, 
    Requests Bem-Sucedidas: ${successfulRequests.count()}, Taxa de Transferência de Dados: ${dataTransferRate.value()} bytes/ms`);
    return {
      "../htmlReport/createUser_testeCarga.html": htmlReport(data),
      
    };
  }


export let options = {
    stages: [
        { duration: '1m', target: 5 }, // Ultilizado para Aumentar Theads dos VUs evitando travamento do sistema
        { duration: '5m', target: 5 }, // Pedido pelo descrição do Cenário
        { duration: '1m', target: 0 }, // Ultilizado para Diminuir Theads dos VUs evitando travamento do sistema

        //{ duration: '5m', target: 5 }, // Pedido pelo descrição do Cenário. 
                                         //Caso desejado comentar os 3 scripts acima e descomenta apenas esse.
    ],
};

// Métricas para monitoramento de desempenho e qualidade das requests
let errorRate = new Rate('errors');
let responseTime = new Trend('response_time');
let successfulRequests = new Counter('successful_requests');
let dataTransferRate = new Gauge('data_transfer_rate');

export default () => {
    const emailGenerator = new RandomEmailGenerator();
    const randomEmail = emailGenerator.generateEmail();
    const url = 'http://localhost:3000/usuarios';
    const headers = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
    };
    const payload = JSON.stringify({
        nome: 'Fulano da Silva',
        email: randomEmail,
        password: 'teste',
        administrador: "true"
    });

    const startTime = new Date().getTime();
    const res = http.post(url, payload, { headers: headers });
    const endTime = new Date().getTime();
    const duration = endTime - startTime;

    // Verifica o status da resposta
    check(res, {
        'status is 201': (r) => r.status === 201,
    });

    // Adiciona às métricas de erro, tempo de resposta, requests bem-sucedidas e taxa de transferência de dados
    errorRate.add(res.status !== 201);
    responseTime.add(duration);
    successfulRequests.add(res.status === 201);
    dataTransferRate.add(res.body.length / (endTime - startTime));


    if (res.status !== 201) {
        console.log(`Falha na request. Status: ${res.status}, Response body: ${res.body}`);
        console.log(`Request falhada: ${payload}`);
    }

    sleep(1);
};