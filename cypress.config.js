const { defineConfig } = require("cypress");
const allureWriter = require('@shelex/cypress-allure-plugin/writer');
module.exports = defineConfig({
  e2e: {
    specPattern: 'cypress/e2e/*/*.cy.js',
    setupNodeEvents(on, config) {
      require('@cypress/grep/src/plugin')(config);
      allureWriter(on, config);
      return config;
    },
  },
  video:false
});
